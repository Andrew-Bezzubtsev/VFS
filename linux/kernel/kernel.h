#ifndef VFS_KERNEL_KERNEL_H
#define VFS_KERNEL_KERNEL_H

#include <linux/types.h>
#include <linux/fs.h>

#define VFS_MAX_DDE_SIZE 32
#define VFS_MAX_DDE_NAME_LENGTH (VFS_MAX_DDE_SIZE - sizeof(__be32))
#define VFS_INODE(inode) ((struct vfs_inode_t *)inode) 
#define VFS_SB(sb) ((struct vfs_super_block_t *)sb)
static const long unsigned VFS_MAGIC = 0x14141414;

struct vfs_disk_super_block_t {
	__be32 magic;
	__be32 block_size;
	__be32 root_inode;
	__be32 inode_blocks;
};

struct vfs_super_block_t {
	long unsigned magic;
	long unsigned inode_blocks;
	long unsigned block_size;
	long unsigned root_inode;
	long unsigned inodes_in_block;
};

struct vfs_inode_t {
	struct inode inode;
	long unsigned block;
};

struct vfs_disk_inode_t {
	__be32 first;
	__be32 blocks;
	__be32 size;
	__be32 gid;
	__be32 uid;
	__be32 mode;
	__be32 ctime;
};

struct vfs_disk_directory_entry_t {
	char name[VFS_MAX_DDE_NAME_LENGTH];
	__be32 inode;
};

extern const struct file_operations VFS_DIR_OPS;
extern const struct inode_operations VFS_DIR_INODE_OPS;
extern const struct file_operations VFS_FILE_OPS;
extern const struct address_space_operations VFS_ADDRESS_SPACE_OPS;

extern struct file_system_type vfs_fs_type;
extern struct kmem_cache *vfs_inode_cache;

struct inode *vfs_inode_get(struct super_block *sb, ino_t inode_id);
struct inode *vfs_inode_alloc(struct super_block *sb);
void vfs_inode_free(struct inode *inode);

#endif /* VFS_KERNEL_KERNEL_H */
