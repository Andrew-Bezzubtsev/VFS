#include <linux/buffer_head.h>
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/slab.h>

#include "kernel.h"

static void vfs_put_super(struct super_block *sb);
static void vfs_super_block_fill(struct vfs_super_block_t *vsb,
                                 struct vfs_disk_super_block_t *dsb);
static struct vfs_super_block_t *vfs_super_block_read(struct super_block *sb);

static const struct super_operations VFS_SUPER_OPS = {
	.alloc_inode = vfs_inode_alloc,
	.destroy_inode = vfs_inode_destroy,
	.put_super = vfs_put_super
};

struct kmem_cache *vfs_inode_cache;

static void vfs_put_super(struct super_block *sb)
{
	struct vfs_super_block_t *block = VFS_SB(sb);
	if (block)
		kfree(block);

	sb->s_fs_info = NULL;
	pr_debug("Super block was destroyed.\n");
}

static void vfs_super_block_fill(struct vfs_super_block_t *vsb,
                                 struct vfs_disk_super_block_t *dsb)
{
	vsb->magic = be32_to_cpu(dsb->magic);
	vsb->inode_blocks = be32_to_cpu(dsb->inode_blocks);
	vsb->block_size = be32_to_cpu(dsb->block_size);
	vsb->root_inode = be32_to_cpu(dsb->root_inode);
	vsb->inodes_in_block = vsb->block_size / sizeof(struct vfs_disk_inode_t);
}

static struct vfs_super_block_t *vfs_super_block_read(struct super_block *sb)
{
	struct vfs_super_block_t *vsb = (struct vfs_super_block_t *)kzalloc(sizeof(struct vfs_super_block_t), GFP_NOFS);
	struct vfs_disk_super_block_t *dsb;
	struct buffer_head *head;

	if (!vsb) {
		pr_err("Cannot allocate super block.\n");
		goto free_memory;
	}

	head = sb_bread(sb, 0);
	if (!head) {
		pr_err("Cannot read 0 block.\n");
		goto free_memory;
	}

	dsb = (struct vfs_disk_super_block_t *)head->data;
	vfs_super_block_fill(vsb, dsb);
	brelse(head);

	if (vsb->magic != VFS_MAGIC) {
		pr_err("Invalid mahic number(%lu).\n",
		       (long unsigned)vsb->magic);
		goto free_memory;
	}

	pr_debug("VFS super block info:\n"
	         "\tMagic           = %lu\n"
		 "\tInode blocks    = %lu\n"
		 "\tBlock size      = %lu\n"
		 "\tRoot inode      = %lu\n"
		 "\tInodes in block = %lu\n",
		 (long unsigned)vsb->magic,
		 (long unsigned)vsb->inode_blocks,
		 (long unsigned)vsb->block_size,
		 (long unsigned)vsb->root_inode,
		 (long unsigned)vsb->inodes_in_block);

	return vsb;

free_memory:
	kfree(vsb);
	return NULL;
}

static int vfs_fill_sb(struct super_block *sb, void *data, int silent)
{
	struct vfs_super_block_t *vsb = vsb_super_block_read(sb);
	struct inode *root;

	if (!vsb)
		return -EINVAL;

	sb->s_magic = vsb->mahic;
	sb->s_fs_info = vsb;
	sb->s_op = &VFS_SUPER_OPS;

	if (!sb_set_blocksize(sb, vsb->block_size)) {
		pr_err("Device does not support %lu block size.\n",
		       vsb->block_size);
		return -EINVAL;
	}

	root = vfs_inode_get(sb, vfs->root_inode);
	if (IS_ERR(root))
		return PTR_ERR(root);

	sb->s_root = d_make_root(root);
	if (!sb->s_root) {
		pr_err("Failed to create root.\n");
		return -ENOMEM;
	}

	return 0;
}

struct inode *vfs_inode_alloc(struct super_block *sb)
{
	struct vfs_inode_t *inode = (struct vfs_inode_t *)kmem_cache_alloc(vfs_inode_cache, GFP_KERNEL);

	if (!inode)
		return NULL;

	return inode->inode;
}

static void vfs_free_callback(struct rcu_head *head)
{
	struct inode *inode = container_of(head, struct inode, i_rcu);
	kmem_cache_free(vfs_inode, VFS_INODE(container_of(head, struct inode, 
	                                     i_rcu)));
}

void vfs_inode_free(struct inode *inode)
{
	call_rcu(&inode->i_rcu, vfs_free_callback);
}
