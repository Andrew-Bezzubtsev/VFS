#include <linux/fs.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/slab.h>

#include "kernel.h"

static void vfs_inode_init_once(void *inode)
{
	inode_init_once(&((struct vfs_inode_t *)inode)->inode);
}

static int vfs_inode_cache_create(void)
{
	vfs_inode_cache = kmem_cache_create("vfs_inode", sizeof(struct vfs_inode_t),
	                                    0, (SLAB_RECLAIM_ACCOUNT | SLAB_MEM_SPREAD),
					    vfs_inode_init_once);

	if (!vfs_inode_cache)
		return -ENOMEM;

	return 0;
}

static void vfs_inode_cache_release(void)
{
	rcu_barrier();
	kmem_cache_destroy(vfs_inode_cache);
	vfs_inode_cache = NULL;
}

static int __init vfs_init(void)
{
	int status;

	status = vfs_inode_cache_create();
	if (status) {
		pr_err("Failed to create inode cache.\n");
		return status;
	}

	status = register_filesystem(&vfs_fs_type);
	if (status) {
		vfs_inode_cache_release();
		pr_err("Failed to register filesystem.\n");
		return status;
	}

	pr_debug("Loaded VFS kernel module.\n");

	return 0;
}

static void __exit vfs_exit(void)
{
	int status;

	status = unregister_filesystem(&vfs_fs_type);
	if (status)
		pr_err("Failed to unregister filesystem.\n");

	vfs_inode_cache_release();
	pr_debug("Unloaded VFS kernel module.\n");
}

module_init(vfs_init);
module_exit(vfs_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Andrew Bezzubtsev");
