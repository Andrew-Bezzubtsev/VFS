#include <linux/aio.h>
#include <linux/buffer_head.h>
#include <linux/mpage.h>
#include <linux/slab.h>

#include "kernel.h"

static void vfs_inode_fill(struct vfs_inode_t *inode,
                           struct vfs_disk_inode_t *disk_inode);
static sector_t vfs_inode_block(struct vfs_super_block_t *sb,
                                ino_t inode);
static size_t vfs_inode_offset(struct vfs_super_block_t *sb,
                               ino_t inode);
struct inode *vfs_inode_get(struct super_block *sb, ino_t inode_id);
static int vfs_get_block(struct inode *inode, sector_t iblock,
                         struct buffer_head *head, int create);
static int vfs_readpage(struct file *file, struct page *page);
static int vfs_readpages(struct file *file, struct address_space *mapping,
                         struct list_head *pages, unsigned pages_amount);
static ssize_t vfs_direct_io(int rw, struct kiocb *iocb,
                             struct iovec *vec, loff_t off,
                             long unsigned nsegs);

const struct address_space_operations VFS_ADDRESS_SPACE_OPS = {
	.readpage = vfs_readpage,
	.readpages = vfs_readpages,
	.direct_IO = vfs_direct_io
};

static void vfs_inode_fill(struct vfs_inode_t *inode,
                           struct vfs_disk_inode_t *disk_inode)
{
	inode->block = be32_to_cpu(disk_inode->first);
	inode->inode.i_mode = be32_to_cpu(disk_inode->mode);
	inode->inode.i_size = be32_to_cpu(disk_inode->size);
	inode->inode.i_blocks = be32_to_cpu(disk_inode->blocks);
	inode->inode.i_ctime.tv_sec = be64_to_cpu(disk_inode->ctime);
	inode->inode.i_mtime.tv_sec = inode->inode.i_atime.tv_sec = inode->inode.i_ctime.tv_sec;
	inode->inode.i_mtime.tv_nsec = inode->inode.i_atime.tv_nsec = inode->inode.i_ctime.tv_nsec = 0;
	
	i_uid_write(&inode->inode, (uid_t)be32_to_cpu(disk_inode->uid));
	i_gid_write(&inode->inode, (gid_t)be32_to_cpu(disk_inode->gid));
}

static sector_t vfs_inode_block(struct vfs_super_block_t *sb,
                                ino_t inode)
{
	return (sector_t)(3 + inode / sb->inodes_in_block);
}

static size_t vfs_inode_offset(struct vfs_super_block_t *sb,
                              ino_t inode)
{
	return (sizeof(struct vfs_disk_inode_t) * (inode % sb->inodes_in_block));
}

struct inode *vfs_inode_get(struct super_block *sb, ino_t inode_id)
{
	struct vfs_super_block_t *vsb;
	struct buffer_head *head;
	struct vfs_disk_inode_t *disk_inode;
	struct vfs_inode_t *inode;
	struct inode *dinode;
	size_t block, offset;

	vsb = VFS_SB(sb);

	dinode = iget_locked(sb, inode_id);
	if (!dinode)
		return ERR_PTR(-ENOMEM);

	if (!(dinode->i_state & I_NEW))
		return dinode;

	inode = VFS_INODE(dinode);
	block = vfs_inode_block(vsb, inode_id);
	offset = vfs_inode_offset(vsb, inode_id);

	pr_debug("Read inode %lu from %lu block with %lu offset.\n",
	         (long unsigned)inode_id, (long unsigned)block, (long unsigned)offset);

	head = sb_bread(sb, block);
	if (!head) {
		pr_err("Cannot read block %lu.\n", (long unsigned)block);
		goto read_failure;
	}

	disk_inode = (struct vfs_disk_inode_t *)(head->b_data + offset);
	vfs_inode_fill(inode, disk_inode);
	brelse(head);

	dinode->i_mapping->a_ops = &VFS_ADDRESS_SPACE_OPS;
	if (S_ISREG(dinode->i_mode)) {
		dinode->i_fop = &VFS_FILE_OPS;
	} else {
		dinode->i_op = &VFS_DIR_INODE_OPS;
		dinode->i_fop = &VFS_DIR_OPS;
	}

	pr_debug("%lu inode information:\n"
	         "\tSize   = %lu\n"
		 "\tBlock  = %lu\n"
		 "\tBlocks = %lu\n"
		 "\tUID    = %lu\n"
		 "\tGID    = %lu\n"
		 "\tMode   = %lo\n",
		 (long unsigned)dinode->i_ino,
		 (long unsigned)dinode->i_size,
		 (long unsigned)inode->block,
		 (long unsigned)dinode->i_blocks,
		 (long unsigned)i_uid_read(dinode),
		 (long unsigned)i_gid_read(dinode),
		 (long unsigned)dinode->i_mode);

	unlock_new_inode(dinode);
	return dinode;

read_failure:
	pr_err("Cannot read inode %lu.\n", (long unsigned)inode_id);
	iget_failed(dinode);

	return ERR_PTR(-EIO);
}

static int vfs_get_block(struct inode *inode, sector_t iblock,
                         struct buffer_head *head, int create)
{
	map_bh(head, inode->i_sb, iblock + VFS_INODE(inode)->block);
	return 0;
}

static int vfs_readpage(struct file *file, struct page *page)
{
	return mpage_readpage(page, vfs_get_block);
}

static int vfs_readpages(struct file *file, struct address_space *mapping,
                         struct list_head *pages, unsigned pages_amount)
{
	return mpage_readpages(mapping, pages, pages_amount, vfs_get_block);
}

static ssize_t vfs_direct_io(int rw, struct kiocb *iocb,
                             struct iovec *vec, loff_t off,
                             long unsigned nsegs)
{
	struct inode *inode = file_inode(iocb->ki_filp);
	return blockdev_direct_IO(rw, iocb, inode, vec, off, 
                                  nsegs, vfs_get_block);
}
