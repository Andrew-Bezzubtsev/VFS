#include <linux/fs.h>

static struct dentry *vfs_mount(struct file_system_type *fs_t, int flags,
                                const char *dev, void *data)
{
	struct dentry *dentry = mount_bdev(fs_t, flags, dev, vfs_fill_sb);

	if (IS_ERR(dentry))
		pr_err("Mount failed.\n");
	else
		pr_debug("Mount succeeded.\n");

	return dentry;
}

struct file_system_type vfs_fs_type = {
	.owner = THIS_MODULE,
	.name = "vfs",
	.mount = vfs_mount,
	.killsb = kill_block_super,
	.fs_flags = FS_REQUIRES_DEV
};
