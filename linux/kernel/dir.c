/* Include needed linux headers */
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/pagemap.h>

/* Include filesystem kernel header */
#include "kernel.h"

/**
 * VFS filename match found structure
 */ 
struct vfs_filename_match_t {
	/**
	 * Directory context of found 
	 * filename
	 */
	struct dir_context context;

	/**
	 * Match's inode
	 * id
	 */
	ino_t ino;
	
	/**
	 * Match's name
	 */
	const char *name;

	/**
	 * Match's name length
	 */
	long len;
};

/**
 * Count directory offset(by directory id)
 */
static size_t vfs_directory_offset(size_t idx);

/**
 * Get amount of pages, taken by directory
 */
static size_t vfs_directory_pages(struct inode *inode);

/**
 * Get directory's entry page
 */
static size_t vfs_directory_entry_page(size_t idx);

/**
 * Get page by inode and id
 */
static struct page *vfs_get_page(struct inode *inode, size_t idx);

/**
 * Write given page
 */
static void vfs_put_page(struct page *page);

/**
 * Add new directory
 */
static int vfs_directory_add(struct dir_context *context,
                             struct vfs_disk_directory_entry_t *entry);

/**
 * Iterate for all directories
 */
static int vfs_iterate(struct inode *inode, struct dir_context *context);

/**
 * Read directory's content
 */
static int vfs_read_directory(struct file *file, struct dir_context *context);

/**
 * Check for match in the directory
 */
static int vfs_match(struct dir_context *context, const char *name, long len,
                     loff_t off, ino_t ino, unsigned type);

/**
 * Get directory inode by its name
 */
static ino_t vfs_inode_by_name(struct inode *directory, struct qstr *srch);

/**
 * Lookup the directory for its
 * entry
 */
static struct dentry *vfs_lookup(struct inode *dir_inode, 
                                 struct dentry *entry,
				 unsigned flags);

/**
 * Transmodule directory operations
 */
const struct file_operations VFS_DIR_OPS = {
	.llseek = generic_file_llseek,
	.read = generic_read_dir,
	.iterate = vfs_read_directory
};

/**
 * Transmodule directory's inode operations
 */
const struct inode_operations VFS_DIR_INODE_OPS = {
	.lookup = vfs_lookup
};

static size_t vfs_directory_offset(size_t idx)
{
	return idx * sizeof(struct vfs_disk_directory_entry_t);
}

static size_t vfs_directory_pages(struct inode *inode)
{
	size_t offset = vfs_directory_offset(inode->i_size);
	return ((offset + PAGE_CACHE_SIZE - 1) >> PAGE_CACHE_SHIFT);
}

static size_t vfs_directory_entry_page(size_t idx)
{
	return vfs_directory_offset(idx) >> PAGE_CACHE_SHIFT;
}

static size_t vfs_directory_entry_offset(size_t idx)
{
	return vfs_directory_offset(idx) - (vfs_directory_entry_page(idx) << PAGE_CACHE_SHIFT);
}

static struct page *vfs_get_page(struct inode *inode, size_t idx)
{
	struct address_space *space = inode->i_mapping;
	struct page *page = read_mapping_page(space, idx, NULL);

	if (!IS_ERR(page))
		kmap(page);

	return page;
}

static void vfs_put_page(struct page *page)
{
	kunmap(page);
	put_page(page);
}

static int vfs_directory_add(struct dir_context *context,
                             struct vfs_disk_directory_entry_t *entry)
{
	unsigned dir_t = DT_UNKNOWN;
	unsigned len = strlen(entry->name);
	size_t no = be32_to_cpu(entry->inode);

	return dir_emit(context, entry->name, len, no, dir_t);
}

static int vfs_iterate(struct inode *inode, struct dir_context *context)
{
	size_t pages = vfs_directory_pages(inode);
	size_t pidx = vfs_directory_entry_page(context->pos);
	size_t offset = vfs_directory_entry_offset(context->pos);

	while (pidx < pages) {
		struct page *page = vfs_get_page(inode, pidx);
		struct vfs_disk_directory_entry_t *entry;
		char *kptr;

		if (IS_ERR(page)) {
			pr_err("Failed to access %zu page of %zu inode!\n",
			       pidx, (size_t)inode->i_ino);

			return PTR_ERR(page);
		}

		kptr = page_address(page);
		entry = (struct vfs_disk_directory_entry_t *)(kptr + offset);

		while (offset < PAGE_CACHE_SIZE && context->pos < inode->i_size) {
			if (!vfs_directory_add(context, entry)) {
				vfs_put_page(page);
				return 0;
			}

			context->pos++;
			entry++;
		}

		vfs_put_page(page);

		pidx++;
		offset = 0;
	}

	return 0;
}

static int vfs_read_directory(struct file *file, struct dir_context *context)
{
	return vfs_iterate(file_inode(file), context);
}


static int vfs_match(struct dir_context *context, const char *name, long len,
                     loff_t off, ino_t ino, unsigned type)
{
	struct vfs_filename_match_t *match = (struct vfs_filename_match_t *)context;

	if (len == match->len && !memcmp(match->name, name, len)) {
		match->ino = ino;
		return 1;
	}

	return 0;
}

static ino_t vfs_inode_by_name(struct inode *directory, struct qstr *srch)
{
	struct vfs_filename_match_t match = {
		{&vfs_match, 0}, 0, srch->name, srch->len
	};

	int state = vfs_iterate(directory, &match.context);
	if (state) {
		pr_err("Not found directory entry '%s'(%d).",
		       srch->name, state);
	}

	return match.ino;
}

static struct dentry *vfs_lookup(struct inode *dir_inode, struct dentry *entry,
                                 unsigned flags)
{
	struct inode *inode;
	ino_t ino;

	if (entry->d_name.len >= VFS_MAX_DDE_NAME_LENGTH)
		return ERR_PTR(-ENAMETOOLONG);

	ino = vfs_inode_by_name(dir_inode, &(entry->d_name));
	if (ino) {
		inode = vfs_inode_get(dir_inode->i_sb, ino);
		if (IS_ERR(inode)) {
			pr_err("Cannot read %lu inode.",
			       (long)ino);
		}

		d_add(entry, inode);
	}

	return NULL;
}
